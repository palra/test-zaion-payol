const util = require('util');

function trailingZero(nb) {
  return `00${nb}`.slice(-2);
}

module.exports = class Moment {
  constructor(opts) {
    if (typeof opts === 'string') {
      if (!/^\d\d:\d\d$/.test(opts)) {
        throw new Error('Invalid moment format : it should be a string of format HH:MM');
      }

      const split = opts.split(':');
      [this.hours, this.minutes] = split;
    } else {
      this.hours = opts.hours;
      this.minutes = opts.minutes;
    }

    this.hours = parseInt(this.hours, 10);
    this.minutes = parseInt(this.minutes, 10);

    this.fixOverflow();
    Object.freeze(this);
  }

  toString() {
    return `${trailingZero(this.hours)}:${trailingZero(this.minutes)}`;
  }

  add(momentLike) {
    let moment = momentLike;
    if (!(moment instanceof Moment)) {
      moment = new Moment(moment);
    }

    return new Moment({
      minutes: this.minutes + moment.minutes,
      hours: this.hours + moment.hours
    });
  }

  compare(moment) {
    const cmpHours = this.hours - moment.hours;
    if (cmpHours !== 0) return cmpHours;
    return this.minutes - moment.minutes;
  }

  equals(moment) {
    return this.compare(moment) === 0;
  }

  gt(moment) {
    return this.compare(moment) > 0;
  }

  geq(moment) {
    return this.compare(moment) >= 0;
  }

  lt(moment) {
    return this.compare(moment) < 0;
  }

  leq(moment) {
    return this.compare(moment) <= 0;
  }

  fixOverflow() {
    while (this.minutes > 60) {
      this.minutes -= 60;
      this.hours += 1;
    }
  }

  [util.inspect.custom]() {
    return this.toString();
  }
};
