const Moment = require('../moment');

test('should parse moments as string', () => {
  const moment = new Moment('08:20');
  expect(moment.hours).toBe(8);
  expect(moment.minutes).toBe(20);
});

test('should parse moments as object', () => {
  const moment = new Moment({
    minutes: 50,
    hours: 18
  });

  expect(moment.hours).toBe(18);
  expect(moment.minutes).toBe(50);
});

test('should add moments', () => {
  const a = new Moment('09:20');
  const b = new Moment('01:30');
  const moment = a.add(b);

  expect(moment.hours).toBe(10);
  expect(moment.minutes).toBe(50);
});

test('should fix overflow', () => {
  const a = new Moment('20:50');
  const b = new Moment('01:15');
  const moment = a.add(b);

  expect(moment.hours).toBe(22);
  expect(moment.minutes).toBe(5);
});

test('should display with trailing zeros', () => {
  let moment = new Moment({
    minutes: 0,
    hours: 5
  });
  expect(moment.toString()).toBe('05:00');

  moment = new Moment({
    minutes: 0,
    hours: 12
  });
  expect(moment.toString()).toBe('12:00');

  moment = new Moment({
    minutes: 30,
    hours: 0
  });
  expect(moment.toString()).toBe('00:30');
});

test('should be immutable', () => {
  const moment = new Moment('08:20');
  moment.hours = 1;
  moment.minutes = 50;

  expect(moment.hours).toBe(8);
  expect(moment.minutes).toBe(20);
});

test('should test comparisons', () => {
  let a = new Moment('20:50');
  let b = new Moment('01:15');

  expect(a.compare(b)).toBeGreaterThan(0);
  expect(a.gt(b)).toBe(true);
  expect(a.leq(b)).toBe(false);

  a = new Moment('12:50');
  b = new Moment('13:30');

  expect(a.compare(b)).toBeLessThan(0);
  expect(a.lt(b)).toBe(true);
  expect(a.geq(b)).toBe(false);

  a = new Moment('15:10');
  b = new Moment('15:10');

  expect(a.compare(b)).toBe(0);
  expect(a.equals(b)).toBe(true);
  expect(a.geq(b)).toBe(true);
  expect(a.leq(b)).toBe(true);
});
