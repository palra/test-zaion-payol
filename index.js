const { some, find, filter } = require('lodash');
const Moment = require('./moment');

const input = `08:20;Paris;Berlin
5
09:20;Paris;Amsterdam;03:20
08:30;Paris;Bruxelles;01:20
10:00;Bruxelles;Amsterdam;02:10
12:30;Amsterdam;Berlin;06:10
11:30;Bruxelles;Berlin;09:20`.split('\n');

// Parsing first line
const split = input[0].split(';');

const departureTime = new Moment(split[0]);
const departureCity = split[1];
const arrivalCity = split[2];

// Parsing train time table
const count = parseInt(input[1], 10);
const trainTimeTable = [];
for (let i = 2; i < count + 2; i += 1) {
  const trainSplit = input[i].split(';');
  const trainRoute = {
    from: trainSplit[1],
    to: trainSplit[2],
    departure: new Moment(trainSplit[0]),
    duration: new Moment(trainSplit[3])
  };

  trainTimeTable.push(trainRoute);
}

// Solving problem
const traversal = [];
traversal.push({
  visited: false,
  city: departureCity,
  time: departureTime,
  from: undefined
});

while (some(traversal, { visited: false })) {
  const traversalIt = find(traversal, { visited: false });
  const { city, time: currentTime } = traversalIt;

  filter(trainTimeTable, { from: city }).forEach(trainRoute => {
    if (currentTime.gt(trainRoute.departure)) {
      return;
    }

    let arrivalCityTraversal = find(traversal, { city: trainRoute.to });
    if (arrivalCityTraversal === undefined) {
      arrivalCityTraversal = {
        city: trainRoute.to
      };

      traversal.push(arrivalCityTraversal);
    }

    arrivalCityTraversal.time = trainRoute.departure.add(trainRoute.duration);
    arrivalCityTraversal.from = city;
    // Mark visited the arrivalCity, as we won't search departures from it.
    arrivalCityTraversal.visited = trainRoute.to === arrivalCity;
  });

  traversalIt.visited = true;
}

const solution = find(traversal, { city: arrivalCity });
if (solution) {
  // eslint-disable-next-line no-console
  console.log(solution.time);
} else {
  // eslint-disable-next-line no-console
  console.error(`Route from ${departureCity} to ${arrivalCity} not found`);
}
